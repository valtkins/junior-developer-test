<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    // Includes FONTS,CSS and META tags to the page 
    @include("inc/header.inc.php");

    // link to classes
    include("inc/model/product_type.inc.php");
    include("inc/model/notification.inc.php");
    include("inc/view/types.inc.php");

    // create objects
    $notification = new notification;
    $types = new viewTypes();


    ?>
    <!-- link to jquery -->
    <script src="js/type_loader.js"></script>


    <title>New product - website</title>
</head>

<body>

    <?php

    // Check for notifications
    if (isset($_GET['action'])) {
        echo $notification->check_notifcation();
    }

    // Include toolbar for website 
    @include("inc/toolbar.inc.php");
    ?>
    <div id="main_body">
        <h1 class='page_title'>Create a product</h1>
        <div class="line line_title"></div>
        <div class="block box_shadow" style='margin-top:10px;'>
            <form action="inc/control/new_product.php" id="add_product" method="POST">
                <p class="small_title">Product information</p>
                <input type="text" class="blue_input input" placeholder="SKU" name="sku" required /><br>
                <input type="text" class="blue_input input" placeholder="Name" name="name" required /><br>
                <input type="number" min="0" step="0.01" class="blue_input input" placeholder="Cost" name="price"
                    required /><span style="margin-left:10px;">EUR</span><br><br>
                <p class="small_title">Type</p>
                <select name="type" class="select" id="type_select">
                    <?php
                    $types->showProductTypes();
                    ?>
                </select>
                <div class="additional_form">

                </div>
                <br>
                <p class="small_title" style='font-size:12px;margin-bottom:5px;'>Make sure ALL fields are fulfilled
                    before creating a new
                    product</p>
                <input type="submit" class="form_submit" value="Create" />
            </form>
        </div>
    </div>
    <script> </script> <!-- To stop css animations from firing on page load - chrome -->
</body>

</html>