// Script for prodocut_list page
// handles checkboxes

//wait for document
$(document).ready(function() {
  // check all checkboxes
  function select_all() {
    $(".checkbox").prop("checked", true);
    // change parent data value to 1 - SELECTED
    $(".checkbox")
      .parent()
      .attr("data-selected", 1);
  }

  // uncheck all checkboxes
  function unselect_all() {
    $(".checkbox").prop("checked", false);
    // change parent data value to 0 - UNSELECTED
    $(".checkbox")
      .parent()
      .attr("data-selected", 0);
  }

  // extra function for when user clicks TEXT instead of checkbox
  function check_change() {
    if ($(".all_checkbox").is(":checked")) {
      $(".all_checkbox").prop("checked", false);
      unselect_all();
    } else {
      $(".all_checkbox").prop("checked", true);
      select_all();
    }
  }

  // change data value of parent if checked
  $(".checkbox").change(function() {
    if ($(this).is(":checked")) {
      $(this)
        .parent()
        .attr("data-selected", 1);
    } else {
      $(this)
        .parent()
        .attr("data-selected", 0);
    }
  });

  // SELECT ALL text is clicked, behave as if checkbox
  $(".select_all_p").click(function() {
    check_change();
  });

  // main checkbox is clicked
  $(".all_checkbox").change(function() {
    if ($(this).is(":checked")) {
      select_all();
    } else {
      unselect_all();
    }
  });
});
