// creates a pop-up when DELETE SELECTED is pressed
$(document).ready(function() {
  $(".delete_selected").click(function() {
    // check if anything is selected
    var selected = false;
    $(".checkbox").each(function(index) {
      if ($(this).is(":checked")) {
        selected = true;
      }
    });

    // if something is selected, fire popup
    if (selected == true) {
      var n = new Noty({
        text: "Are you sure?",
        theme: "relax",
        closeWith: "button",
        layout: "center",
        buttons: [
          Noty.button(
            "DELETE",
            "btn btn-success",
            function() {
              // click delete button <------------

              var id_list = "0";

              // check which boxes are checked
              $('[data-selected!="0"]').each(function(index) {
                if ($(this).attr("data-f_id")) {
                  // add checked IDs to list
                  id_list = id_list + "," + $(this).attr("data-f_id");
                }
              });

              // send ID list to backend
              $.ajax({
                method: "POST",
                url: "inc/control/mass_delete.inc.php",
                data: {
                  ids: id_list
                }
              }).done(function(msg) {
                // check to see if went through
                if (msg !== "error") {
                  //refresh page if all is well

                  location.reload();
                } else {
                  alert("Something went wrong");
                  console.log(msg);
                }
              });
            },
            { id: "button1", "data-status": "ok" }
          ),

          Noty.button("CANCEL", "btn btn-error", function() {
            n.close();
          })
        ]
      });
      n.show();
    }
    //if nothing selected, make popup
    else {
      new Noty({
        type: "warning",
        text: "NOTHING SELECTED",
        timeout: 5000,
        layout: "topRight",
        theme: "relax"
      }).show();
    }
  });
});
