// form information for product types
function get_type_form(sent_type) {
  $.ajax({
    method: "POST",
    url: "inc/control/product_type.inc.php",
    data: {
      type: sent_type
    }
  }).done(function(msg) {
    // check to see if there is anything to load
    if (msg !== "error") {
      //load information
      $(".additional_form").html(msg);
    }
  });
}

// wait for document to load
$(document).ready(function() {
  // get the already chosen option
  var pre_chosen = $("#type_select")
    .children("option:selected")
    .val();

  // load information from the chosen option
  get_type_form(pre_chosen);

  // check to see if chosen option changes
  $("#type_select").change(function() {
    var chosen = $(this)
      .children("option:selected")
      .val();

    // change the additional form
    get_type_form(chosen);
  });
});
