<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    // Includes FONTS,CSS and META tags to the page 
    @include("inc/header.inc.php");

    // link to classes

    include("inc/model/notification.inc.php");
    include("inc/model/product.inc.php");
    include("inc/view/products.inc.php");
    // create objects
    $notification = new notification;
    $products = new productView;

    ?>

    <title>All products - website</title>
</head>

<body>
    <!-- checkbox handler -->
    <script src="js/product_select.js"></script>

    <!-- mass_delete -->
    <script src="js/mass_delete.js"></script>
    <?php

    // Check for notifications
    if (isset($_GET['action'])) {
        echo $notification->check_notifcation();
    }

    // Include toolbar for website 
    @include("inc/toolbar.inc.php");

    ?>
    <div id="main_body">
        <h1 class='page_title'>Products</h1>
        <div class="line line_title"></div>
        <div class="option_holder">
            <input type='checkbox' class='all_checkbox' />
            <p class='no_select select_all_p'>Select all</p>
            <p class='delete_selected no_select'><i class="fas fa-trash"></i> DELETE SELECTED</p>
        </div>
        <div class="block box_shadow product_holder" style='margin-top:10px;'>
            <?php
            $products->showProducts();
            ?>
        </div>
    </div>
    <script> </script> <!-- To stop css animations from firing on page load - chrome -->
</body>

</html>