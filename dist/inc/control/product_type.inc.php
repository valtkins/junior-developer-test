<?php
// check wether user input exists
if (isset($_POST['type'])) {
    // classes
    include("../database.inc.php");
    include("../model/product_type.inc.php");

    // clean user input
    $type = new productTypes;
    $output = $type->cleanInput($_POST['type']);

    // check if input exists as type and output
    if (file_exists("../../types/$output.html")) {
        include("../../types/$output.html");
    }
}