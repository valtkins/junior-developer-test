<?php
// check wether user input exists
if (isset($_POST['ids'])) {
    // classes
    include("../database.inc.php");


    // clean user input
    $db = new db;
    $output = $db->cleanInput($_POST['ids']);

    $list = explode(",", $output);

    // clean the user input in-case they edited the HTML
    $new_list = "0"; # makes code shorter, no product ID is 0
    foreach ($list as $id) {
        if (is_numeric($id) && $id !== "0") {
            $new_list .= "," . $id;
        }
    }
    // delete all products with corresponding IDs
    $sql = "DELETE from products WHERE id IN ($new_list)";
    $res = $db->connect()->query($sql);

    // in case of error
    if ($res) {
    } else {
        echo "error";
    }
}