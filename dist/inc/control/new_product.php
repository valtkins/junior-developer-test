<?php
//link to classes
include("../database.inc.php");
include("../model/product.inc.php");

// create object
$db = new db;
$product = new product;

// get all PRODUCT table rows
$data = $db->check_rows("products");

// check if $_POST[] match $data (we dont want unnecessary data being saved)
$optimized = $product->optimize_product($data);

if ($optimized == "error") {
    //display error message
    header("Location: ../../new_product.php?action=error");
} else {
    // create product and display success message
    $db->insert("products", $optimized);
    header("Location: ../../new_product.php?action=success");
}
