<?php

class notification extends db
{
    public function check_notifcation()
    {
        if (isset($_GET['action'])) {
            $clean = $this->cleanInput($_GET['action']);
            if ($clean == "error" || $clean == "success") {
                $this->send_notification($clean);
            }
        }
    }

    public function send_notification($type)
    {
        if ($type == "error") {
            $this->create_notification("warning", "5000", "ERROR CREATING PRODUCT!");
        } else {
            $this->create_notification("success", "5000", "PRODUCT CREATED!");
        }
    }

    public function create_notification($type, $timeout, $text)
    {
        echo "
        <script>
        function showNotification(add) {
            new Noty({
                type: \"$type\",
                text: add,
                timeout: $timeout,
                layout: \"topRight\",
                theme: \"relax\"
            }).show();
        };
        showNotification('$text');
        </script>
        ";
    }
}