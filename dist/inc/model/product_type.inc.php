<?php

class productTypes extends db
{
    protected function getProductTypes()
    {
        $sql = "SELECT name FROM types";
        $res = $this->connect()->query($sql);
        if ($res) {
            while ($row = $res->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function cleanType($type)
    {
        $selected_type = htmlspecialchars($this->connect()->real_escape_string($type));
        return $selected_type;
    }
}