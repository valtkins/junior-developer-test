<?php

class product extends db
{

    protected function getProducts()
    {
        $sql = "SELECT * FROM products";
        $res = $this->connect()->query($sql);
        if ($res) {
            while ($row = $res->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function optimize_product($data)
    {
        $product_data = array();
        foreach ($data as $column) {
            // look for user input

            if (isset($_POST[$column])) {
                $user_input = $_POST[$column];
                // check if input is a string
                if (is_string($user_input)) {
                    $cleaned = $this->cleanInput($user_input);

                    // check if type exists so the user cant make their own new types
                    if ($column == "type") {
                        if (mysqli_num_rows($this->find_row("types", "name='$cleaned'")) <= 0) {
                            return "error"; // means that the user tried to change the type
                        }
                    }

                    $product_data[$column] = $cleaned;
                } else {
                    $count = count($user_input);
                    if ($count == 3) {
                        $text = implode("x", $user_input);
                        $product_data[$column] =  $text;
                    }
                }
            } else {
                if ($column !== "id") {
                    return "error"; // this means the user tried adding more data than the form
                }
            }

            // add input to $data table
        }
        return $product_data;
    }
}
