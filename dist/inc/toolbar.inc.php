<div id="toolbar" class='box_shadow'>
    <img src="img/logo.png" alt="Logo" class="logo_image">


    <a href='product_list' class='toolbar_link no_select'><i class="fas fa-store-alt"></i> Products</a>
    <div class="side_line"></div>
    <a href='new_product' class='toolbar_link no_select'><i class="fas fa-cart-arrow-down"></i> Create product</a>

</div>