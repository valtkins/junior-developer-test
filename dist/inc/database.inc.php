<?php

class db
{
    private $servername;
    private $username;
    private $password;
    private $dbname;


    // connect to database
    public function connect()
    {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->dbname = "productwebsite";

        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        return $conn;
    }

    // insert new row 
    public function insert($table_name, $data)
    {
        // convert data table to string
        $sql = "INSERT INTO $table_name (";
        $sql .= implode(",", array_keys($data)) . ") VALUES ('";
        $sql .= "" . implode("','", array_values($data)) . "')";

        // check if query can be executed
        if (mysqli_query($this->connect(), $sql)) {
            return true;
        } else {
            echo mysqli_error($this->connect());
        }
    }
    public function check_rows($table_name)
    {

        $string = "SHOW COLUMNS FROM $table_name";

        $res = $this->connect()->query($string);
        if ($res) {
            while ($row = $res->fetch_assoc()) {
                $data[] = $row['Field'];
            }
            return $data;
        }
    }

    public function cleanInput($input)
    {
        $cleaned = htmlspecialchars($this->connect()->real_escape_string($input));
        return $cleaned;
    }

    public function find_row($table, $search_what)
    {
        $sql = "SELECT * FROM $table WHERE $search_what";
        $line = mysqli_query($this->connect(), $sql);
        return $line;
    }
}