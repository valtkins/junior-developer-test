<?php

class viewTypes extends productTypes
{
    public function showProductTypes()
    {
        $data = $this->getProductTypes();
        foreach ($data as $type) {
            echo "<option value='" . $type['name'] . "'>" . $type['name'] . "</option>";
        }
    }
}