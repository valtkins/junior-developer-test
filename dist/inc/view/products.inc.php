<?php

class productView extends product
{
    public function showProducts()
    {
        $data = $this->getProducts();
        foreach ($data as $item) {

            //find type prefix and prefix name
            $type = $item['type'];
            $row = $this->find_row("types", "name='$type'");
            $row = $row->fetch_assoc();
            if ($row) {
                $prefix = $row['prefix']; // this would be (MB,KG,M)
                $prefix_name = $row['prefix_name']; // this would be (Size,Weight,Dimensions)
            }

            echo " <div data-selected='0' data-f_id='" . $item['id'] . "'class=\"product_item box_shadow\">
           
            <h3 class=\"sku_display\">" . $item['sku'] . "</h3>
            <div style='background-color:#555;margin-top:10px;'class='line'></div>
            <h3 class=\"name_display\">" . $item['name'] . "</h3>
            <h3 class=\"cost_display\">" . $item['price'] . " EUR</h3>
            <h3 class=\"attribute_display\">" . $prefix_name . ": <span style='color:#42a1f2;'>" . $item['additional'] . "</span> " . $prefix . "</h3>
            <input type='checkbox' class='checkbox' />
        </div>";
        }
    }
}